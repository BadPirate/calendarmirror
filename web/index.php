<?php

require('../vendor/autoload.php');
require_once 'fetchCalendar.inc';

use Symfony\Component\HttpFoundation\Request;

$app = new Silex\Application();
$app['debug'] = true;

// Register the monolog logging service
$app->register(new Silex\Provider\MonologServiceProvider(), array(
  'monolog.logfile' => 'php://stderr',
));

function debug($text) {
  global $app;
  $app['monolog']->addDebug($text);
}

function addMirror(Request $request, $app) {
  // Fetch the calendar
  $name = $request->request->get('name');
  if(!isset($name)) { return "Must set a calendar name"; }
  $url = $request->request->get('subscription');
  debug("Fetching calendar at $url");
  $vcalendar = subscriptionCalendar($url);
  if(!isset($vcalendar)) {
    return "Unable to read calendar at $url";
  }
  
  // Get the refresh token
  $accountid = $request->request->get('account');
  debug("Account ID - $accountid");
  $refreshToken = refreshTokenForCalendar($accountid, $app);
  debug("Refresh Token - $refreshToken");
  if(!isset($refreshToken)) { return "Unable to connect to Google Accounts."; }
  
  // Create the new calendar
  $client = client();
  $client->refreshToken($refreshToken);
  $service = new Google_Service_Calendar($client);
  $calendar = new Google_Service_Calendar_Calendar();
  $calendar->setSummary($name);
//  $calendar->setTimeZone('America/Los_Angeles');
  $createdCalendar = $service->calendars->insert($calendar);
  $calendarID = $createdCalendar->getId();
  if(!isset($calendarID)) { return "Error creating new calendar"; }
  debug("Calendar created - $calendarID");
  
  // Add calendar to mirror to database
  $stmt = $app['pdo']->prepare("INSERT INTO mirrors (name, subscription, destination, credential_id) VALUES (:name, :subscription, :destination, :credential_id)");
  $stmt->execute([':name' => $name, ':subscription' => $url, ':destination' => $calendarID, ':credential_id' => $accountid]);
  if($stmt->errorCode() != 0) { return print_r($stmt->errorInfo(),true); }
  
  // Kick off a sync
  syncCalendar($vcal, $client, $calendarID, $app);
  
  return $app->redirect('/');
}

// Register view rendering
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views',
));

// PDO
$dbopts = parse_url(getenv('DATABASE_URL'));
$app->register(new Herrera\Pdo\PdoServiceProvider(),
               array(
                   'pdo.dsn' => 'pgsql:dbname='.ltrim($dbopts["path"],'/').';host='.$dbopts["host"] . ';port=' . $dbopts["port"],
                   'pdo.username' => $dbopts["user"],
                   'pdo.password' => $dbopts["pass"]
               )
);


// Google
function client() {
  $client = new Google_Client();
  $client->setAuthConfigFile('../client_secret.json');
  $client->addScope('https://www.googleapis.com/auth/calendar');
  $client->addScope('https://www.googleapis.com/auth/userinfo.email');
  $client->setAccessType("offline");
  $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/callback.php');
  return $client;
}

function getAccounts($app) {
  $st = $app['pdo']->prepare('SELECT * FROM credentials');
  $st->execute();
  if($st->errorCode() != 0) { return print_r($st->errorInfo(),true); }

  $accounts = array();
  while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $accounts[] = $row;
  }
  return $accounts;
}

function getMirrors($app) {
  $st = $app['pdo']->prepare('SELECT * FROM mirrors');
  $st->execute();
  if($st->errorCode() != 0) { return print_r($st->errorInfo(),true); }

  $mirrors = array();
  while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $mirrors[] = $row;
  }
  return $mirrors;
}

// Our web handlers
$app->get('/', function() use($app) {  
  debug('logging output.');


  return $app['twig']->render('index.twig', ['accounts' => getAccounts($app),
      'mirrors' => getMirrors($app)
    ]);
});

$app->get('/sync/{mirrorid}', function($mirrorid) use($app) { 
  $st = $app['pdo']->prepare('SELECT m.subscription as subscription, m.destination as destination, c.auth as auth FROM mirrors as m, credentials as c WHERE m.id = :mirrorid AND m.credential_id = c.id');
  $st->execute([':mirrorid' => $mirrorid]);
  if($st->errorCode() != 0) { return print_r($st->errorInfo(),true); }
  $result = $st->fetch(PDO::FETCH_ASSOC);
  $url = $result['subscription'];
  if(!isset($url)) { return "Unable to lookup subscription URL"; }
  $vcal = subscriptionCalendar($url);
  $client = client();
  $refreshToken = $result['auth'];
  $client->refreshToken($refreshToken);
  $calendarID = $result['destination'];
  syncCalendar($vcal, $client, $calendarID, $app);
  return $app->redirect('/');
});

$app->get('/delete/credential/{accountid}', function($accountid) use($app) {  
  $st = $app['pdo']->prepare('DELETE FROM credentials WHERE id = :accountid');
  debug("Deleting credential #".$accountid);
  $st->execute([':accountid' => $accountid]);
  if($st->errorCode() != 0) { return print_r($st->errorInfo(),true); }
  return $app->redirect('/');
});

$app->get('/connect', function() use($app) {  
 $client = client();
  $auth_url = $client->createAuthUrl();
  return $app->redirect($auth_url);
});

$app->post('/add/mirror', function(Request $request) use($app) {
  return addMirror($request, $app);
});

function refreshTokenForCalendar($accountid, $app) {
  $stmt = $app['pdo']->prepare("SELECT auth FROM credentials WHERE id = :accountid");
  $stmt->execute([':accountid' => $accountid]);
  if($stmt->errorCode() != 0) { return print_r($stmt->errorInfo(),true); }
  $credential = $stmt->fetch(PDO::FETCH_ASSOC);
  $refreshToken = $credential['auth'];
  debug("Refresh Token - $refreshToken");
  return $refreshToken;
}

$app->get('/callback.php', function(Request $request) use($app) {
  $app['monolog']->addDebug('logging output.');
  $error = $request->query->get('error_description');
  if (isset($error)) {
    return $error;
  }
  $code = $request->query->get('code');
  $app['monolog']->addDebug("Code - '$code'");
  $client = client();
  $accessToken = $client->authenticate($code);
  debug("Access token - '$accessToken'");
  $client->setAccessToken($accessToken);
  $refreshToken = $client->getRefreshToken();
  debug("Refresh token - $refreshToken");
  $oauth2 = new Google_Service_Oauth2($client);
  $userInfo = $oauth2->userinfo->get();
  $name = $userInfo['name'] . ' - ' . $userInfo['email'];
  $stmt = $app['pdo']->prepare("INSERT INTO credentials (name, auth) VALUES (:name, :auth)");
  $stmt->execute(array( ':name' => $name, ':auth' => $refreshToken));
  if($stmt->errorCode() != 0) { return print_r($stmt->errorInfo(),true); }
  debug("Insert into DB - Result: " . $stmt->errorCode());
  return $app->redirect('/');
});

$app->get('/db/', function() use($app) {
  $st = $app['pdo']->prepare('SELECT name FROM test_table');
  $st->execute();

  $names = array();
  while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $app['monolog']->addDebug('Row ' . $row['name']);
    $names[] = $row;
  }

  return $app['twig']->render('database.twig', array(
    'names' => $names
  ));
});

$app->get('/schema/{version}', function ($version) use ($app) {
    $path = '../schema/' . $version;
    if (!file_exists($path)) {
        $app->abort(404);
    }

    return $app->sendFile($path);
});

$app->run();
