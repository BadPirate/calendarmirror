<?php

use Sabre\VObject;

require('../vendor/autoload.php');

function curl_get_contents($url)
{
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

function subscriptionCalendar($url) {
  return VObject\Reader::read(curl_get_contents($url));
}

function googleCalendar($client, $id) {
  $service = new Google_Service_Calendar($client);
  $calendar = $service->calendars->get($id);
  return $calendar;
}

function syncCalendar($vcal, $client, $calendarID, $app) {
  $service = new Google_Service_Calendar($client);
  $format = 'Y-m-d\TH:i:sP';
  $formatShort = 'Y-m-d';
  
  // Index VCAL Events
  $vcalEvents = array();
  foreach($vcal->VEVENT as $event) {
    $summary = "";
    if (isset($event->SUMMARY)) { $summary = $event->SUMMARY->getValue(); }
    $description = "";
    if (isset($event->DESCRIPTION)) { $description = $event->DESCRIPTION->getValue(); }
    $startTimestamp = $event->DTSTART->getDateTime()->format($formatShort);
    $endTimestamp = $event->DTEND->getDateTime()->format($formatShort);
    $hash = eventHash($summary, $description, $startTimestamp, $endTimestamp);
    $vcalEvents[$hash] = $event;
  }
  
  echo "<BR><BR>";
  
  // Index GCAL Events
  $gcalEvents = array();
  $events = $service->events->listEvents($calendarID);
  while(true) {
    foreach ($events->getItems() as $event) {
      $summary = $event->getSummary();
      $description = $event->getDescription();
      $startTimestamp = "";
      if(isset($event->getStart()->date)) {
        $startTimestamp = $event->getStart()->date;
      }
      else {
        $startTimestamp = $event->getStart()->dateTime;
      }
      $endTimestamp = "";
      if(isset($event->getEnd()->date)) {
        $endTimestamp = $event->getEnd()->date;
      }
      else {
        $endTimestamp = $event->getEnd()->dateTime;
      }
      $hash = eventHash($summary, $description, $startTimestamp, $endTimestamp);
      if(isset($gcalEvents[$hash])) {
        echo "Deleting duplicate event - $eventID<BR>";
        $service->events->delete($calendarID, $event->getId());
      }
      else
      {
        $gcalEvents[$hash] = $event;
      }
    }
    $pageToken = $events->getNextPageToken();
    if ($pageToken) {
      $optParams = array('pageToken' => $pageToken);
      $events = $service->events->listEvents('primary', $optParams);
    } else {
      break;
    }
  }

  // Insert missing
  $vcalUIDs = array_keys($vcalEvents);
  $gcalUIDs = array_keys($gcalEvents);
  $toInsert = array_diff($vcalUIDs, $gcalUIDs);
    
  echo "<pre>".print_r($toInsert,true)."</pre>";
  
  foreach ($toInsert as $insertHash) {
    if (!isset($insertHash)) {
      continue;
    }
    $ve = $vcalEvents[$insertHash];
    $start = $ve->DTSTART->getValue();
    $startType = 'dateTime';
    if(strlen($start) <= 8) {
      $start = $ve->DTSTART->getDateTime()->format($formatShort);
      $startType = 'date';
    }
    $end = $ve->DTEND->getValue();
    $endType = 'dateTime';
    if(strlen($end) <= 8) { 
      $endType = 'date'; 
      $end = $ve->DTEND->getDateTime()->format($formatShort);
    }
    $event = new Google_Service_Calendar_Event([
      'start' => [$startType => $start],
      'end' => [$endType => $end],
    ]);
    if(isset($ve->SUMMARY)) { $event->setSummary($ve->SUMMARY->getValue()); }
    if(isset($ve->LOCATION)) { $event->setLocation($ve->LOCATION->getValue()); }
    if(isset($ve->DESCRIPTION)) { $event->setDescription($ve->DESCRIPTION->getValue); }
    $event = $service->events->insert($calendarID, $event);
  }
  
  $toDelete = array_diff($gcalUIDs,$vcalUIDs);
  foreach ($toDelete as $deleteHash) {
    $event = $gcalEvents[$deleteHash];
    $eventID = $event->getId();
    echo "Deleting no longer existant event - $eventID<BR>";
    $service->events->delete($calendarID, $eventID);
  }
  
  // TODO: Update Existing
  
  // TODO: Delete missing
  
  $stmt = $app['pdo']->prepare("UPDATE mirrors SET last_sync = NOW() WHERE destination = :calendarid");
  $stmt->execute([':calendarid' => $calendarID]);
  
  exit(1);
}

function eventHash($summary, $description, $startTimestamp, $endTimestamp) {
  $hashString = "$summary && $description && $startTimestamp && $endTimestamp";
  $result = crc32($hashString);
  echo "$result = $hashString<br>";
  return $result;
}
//$name = "AirBNB Tahoe";
//$refreshToken = "1/rAZiFw3qyZCKfp6nQHBEbTcEZN7QiUE5whbo-forV_w";
//$calendarID = NULL;
//
//$vcalendar = VObject\Reader::read(
//    curl_get_contents('https://www.airbnb.com/calendar/ical/11812878.ics?s=224682666d7ed45ee09f21636e2c312e')
//);
//

//
//echo count($vcalEvents)."\n\n";
//
//$client = client();
//$client->fetchAccessTokenWithRefreshToken($refreshToken);
//
//if (!isset($calendarID)) {
//  $service = new Google_Service_Calendar($client);
//  $calendar = new Google_Service_Calendar_Calendar();
//  $calendar->setSummary('New Calendar');
//  $calendar->setTimeZone('America/Los_Angeles');
//
//  $createdCalendar = $service->calendars->insert($calendar);
//
//  $calendarID = $createdCalendar->getId();
//}
//
//echo "$calendarID\n";