<?php
require __DIR__ . '/vendor/autoload.php';

include_once("GoogleCalendar.inc");

use Sabre\VObject;

abstract class Errors
{
  const SourceError = 0;
  // etc.
}

function info($message) {
  echo "$message\n";
}

function curl_get_contents($url)
{
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

$CALENDAR_ADDRESS = 'https://www.airbnb.com/calendar/ical/11812878.ics?s=224682666d7ed45ee09f21636e2c312e';

info("Downloading calendar");
$data = curl_get_contents($CALENDAR_ADDRESS);
if (!$data) {
  error_log("Unable to download calendar $CALENDAR_ADDRESS");
  exit(Errors::SourceError);
}
info("Read ".strlen($data)." bytes.");

info("Creating vCalendar");
$vcalendar = VObject\Reader::read($data, VObject\Reader::OPTION_FORGIVING);

info("Connecting to Google Calendar");
?>